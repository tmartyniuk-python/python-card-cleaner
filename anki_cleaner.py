import sys
import os
import argparse
import re

def main():
    os.system('echo ECHO')

    parser = argparse.ArgumentParser(description='deletes the ==Etymology== section from anki txt card file')
    parser.add_argument('ankifile')
    parser.add_argument('outfile')


    args = parser.parse_args()
    print(args)

    if not os.path.exists(args.ankifile):
        sys.exit('the file does not exist: {}'.format(args.ankifile))

    new_contents = None
    with open(args.ankifile, 'r', encoding="utf8") as ankifile_read:

        etymology_header_pattern = '(===Etymology===(<br>)*(.+?))<br>'
        contents = ankifile_read.read()

        new_contents = re.sub(etymology_header_pattern, "", contents)

    with open(args.outfile, 'w', encoding="utf8") as ankifile_write:
        ankifile_write.write(new_contents)


def deprecation_warning_pattern():
    return '''Subscribe to the mediawiki-api-announce mailing list at <https://lists.wikimedia.org/mailman/listinfo/mediawiki-api-announce>
    for notice of API deprecations and breaking changes. Use Special:ApiFeatureUsage to see usage of deprecated features by your application.
        </main><revisions xml:space="preserve">Because "rvslots" was not specified, 
        a legacy format has been used for the output. This format is deprecated, 
        and in the future the new format will always be used.</revisions></warnings><query><pages>
        <page _idx="10032" pageid="10032" ns="0" title="hem"><revisions><rev contentformat="text/x-wiki" 
        contentmodel="wikitext" xml:space="preserve">'''


if __name__ == "__main__":
    main()